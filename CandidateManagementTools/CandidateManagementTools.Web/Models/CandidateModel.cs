﻿using CandidateManagementTools.Entity;
using System;
using System.ComponentModel.DataAnnotations;

namespace CandidateManagementTools.Web.Models
{
    public class CandidateModel
    {
        [Display(Name = "Candidate Id")]
        public int Id { get; set; }

        [Display(Name = "Candidate Name")]
        [Required(ErrorMessage = "Candidate Name is required")]
        [RegularExpression(@"^[A-Za-z ,.'-]+$", ErrorMessage = "Candidate Name should alphabetic only.")]
        public string Name { get; set; }

        [Display(Name = "Candidate Surname")]
        [RegularExpression(@"^[A-Za-z ,.'-]+$", ErrorMessage = "Candidate Surname should alphabetic only.")]
        public string Surname { get; set; }

        [Display(Name = "Position")]
        public string Position { get; set; }

        [Display(Name = "Curriculum Vitae")]
        public string CurriculumVitaePath { get; set; }

        public Boolean IsActive { get; set; }
    }
}