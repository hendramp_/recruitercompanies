﻿(function () {
    'use strict';

    angular.module('candidateManagement', ['common.core', 'common.ui'])
        .config(config);

    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "scripts/spa/home/index.html",
                controller: "indexCtrl"
            })
            .when("/candidates", {
                templateUrl: "scripts/spa/candidates/candidates.html",
                controller: "candidatesCtrl"
            }).otherwise({ redirectTo: "/" });
    }

})();