﻿using System;

namespace CandidateManagementTools.Entity
{
    public class Position : IEntity
    {
        public int Id { get; set; }
        public string PositionName { get; set; }
        public Boolean IsActive { get; set; }
    }
}
