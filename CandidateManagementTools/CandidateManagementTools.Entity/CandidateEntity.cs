﻿using System;

namespace CandidateManagementTools.Entity
{
    public class Candidate : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Position { get; set; }
        public string CurriculumVitaePath { get; set; }
        public Boolean IsActive { get; set; }
    }
}
