﻿using CandidateManagementTools.Entity;
using System.Data.Entity.ModelConfiguration;

namespace CandidateManagementTools.Data.Configuration
{
    public class EntityConfiguration<T> : EntityTypeConfiguration<T> where T : class, IEntity
    {
        public EntityConfiguration()
        {
            HasKey(e => e.Id);
        }
    }
}
