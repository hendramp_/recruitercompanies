﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CandidateManagementTools.Entity;

namespace CandidateManagementTools.Data.Configuration
{
    public class PositionConfiguration : EntityConfiguration<Position>
    {
        public PositionConfiguration()
        {
            Property(c => c.PositionName).IsOptional().HasMaxLength(45);
            Property(c => c.IsActive).IsOptional();
        }
    }
}
