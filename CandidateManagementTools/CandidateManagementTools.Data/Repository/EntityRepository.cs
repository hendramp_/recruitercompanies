﻿using CandidateManagementTools.Data.Core;
using CandidateManagementTools.Entity;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace CandidateManagementTools.Data.Repository
{
    public class EntityRepository<T> : IEntityRepository<T>  where T : class, IEntity, new()
    {
        private CandidateManagementToolsContext dataContext;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected CandidateManagementToolsContext DbContext
        {
            get { return dataContext ?? (dataContext = DbFactory.Init()); }
        }

        public EntityRepository(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
        }
        
        public virtual IQueryable<T> GetAll()
        {
            return DbContext.Set<T>();
        }

        public T GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public virtual void Create(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry<T>(entity);
            DbContext.Set<T>().Add(entity);
        }

        public virtual void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }
    }
}
