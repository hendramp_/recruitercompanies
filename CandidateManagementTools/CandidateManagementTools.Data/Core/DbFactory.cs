﻿namespace CandidateManagementTools.Data.Core
{
    public class DbFactory : Disposable, IDbFactory
    {
        CandidateManagementToolsContext dbContext;

        public CandidateManagementToolsContext Init()
        {
            return dbContext ?? (dbContext = new CandidateManagementToolsContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null) { dbContext.Dispose(); };               
        }
    }
}
