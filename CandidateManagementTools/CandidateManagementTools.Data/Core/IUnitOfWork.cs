﻿namespace CandidateManagementTools.Data.Core
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
